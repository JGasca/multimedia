package Principal;
/**
 * Clase Parametros que contiene el nivel en el que estamos,la vida con la que empiezan los enemigos, la vida de la nave principal y el numero de enemigos
 * @author javig
 *
 */
public class Parametros {
	public static int nivel = 1;
	private static float vidaEnemigos = 20;
	private static float vidaNave = 50;
	public static int numEnemigos = 4;
	
	
	/**
	 * Metodo que devuelve la vida de la nave principal
	 * @return La vida de la nave principal
	 */
	public static float getVidaNave() {
		return vidaNave;
	}
	/**
	 * Metodo que establece la vida de la nave principal
	 * @param vidaNave Vida de la nave principal
	 */
	public static void setVidaNave(float vidaNave) {
		Parametros.vidaNave = vidaNave;
	}
	/**
	 * Meotodo que devuelve la vida de las naves enemigas
	 * @return Vida de las naves enemigas
	 */
	public static float getVidaEnemigos() {
		return vidaEnemigos;
	}
	/**
	 * Meotodo que establece la vida de la naves enemigas
	 * @param vidaEnemigos Vida de las naves enemigas
	 */
	public static void setVidaEnemigos(float vidaEnemigos) {
		Parametros.vidaEnemigos = vidaEnemigos;
	}
	/**
	 * Metodo que suma uno a la variable nivel
	 */
	public static void SumUnoNivel() {
		nivel++;
	}
	/**
	 * Metodo que resetea todas las variable por defecto
	 */
	public static void resetParametros() {
		nivel = 1;
		vidaEnemigos = 20;
		vidaNave = 50;
		numEnemigos = 4;
	}
}
