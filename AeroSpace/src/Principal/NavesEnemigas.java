package Principal;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
/**
 * Clase NavesEnemigas que contiene todo el comportamiento de las naves enemigas del juego
 * @author javig
 *
 */
public class NavesEnemigas extends Enemigo{
	private Animation nave;
	private float vida;
	
	private int naveSeleccionada;
	
	private float aceleracion;
	private float aceleracionPersigue;
	private float velocidadMaxima;
	private int numero;
	private float distanciaVision;
	private float inicioX;
	private float inicioY;
	
	private float tiempoDisparo;
	private float cuentaDisparo;
	private ArrayList<Bala> balas;
	private int numBalas;
	private int balaActual;
	private float coolDownBala;
	private float cuentaCoolDownBala;
	
	private float tiempoComportamiento;
	private float delayMovimiento;
	
	private boolean enabled;
	
	private float cuentaInvulnerable;
	private float tiempoInvulnerable; 
	
	private boolean muerta;
	
	public float dano;
	
	private boolean derecha;
	private boolean izquierda;
	private boolean pari;
	
	int con = 0;
	
	/**
	 * Constructor de la clase NavesEnemigas que inicializa todas las variables, posiciona la nave enemiga y establece la animaci�n
	 * @param x Posici�n en x del enemigo
	 * @param y Posici�n en y del enemigo
	 * @param s Stage del nivel
	 * @param nivel LevelScreen del nivel
	 * @param vida vida del enemigo
	 * @param nave Numero de la nave que corresponde con un comportamiento y una animaci�n diferentes
	 */
	public NavesEnemigas(float x, float y, Stage s, LevelScreen nivel, float vida,int nave) {
		super(x, y, s, nivel);
		this.naveSeleccionada = nave;
		this.inicioX=x;
		this.inicioY=y;
		this.vida = vida;
		enabled = true;
		muerta = false;
		tiempoComportamiento = 5.5f;
		delayMovimiento =0.2f;
		aceleracion = 5;
		aceleracionPersigue = 40;
		dano = 10f;
		velocidadMaxima = 20;
		
		derecha = true;
		izquierda = false;
		
		cuentaInvulnerable=0;
		tiempoInvulnerable=0.3f;
		
		
		this.cuentaDisparo=0;
		this.tiempoDisparo=1;
		pari = true;
		
		switch (naveSeleccionada) {
		case 1:
			String[] nave1 = {"assets/enemigos/Enemigo01.png","assets/enemigos/Enemigo01_2.png"};
			this.nave = loadAnimationFromFiles(nave1, 0.2f, true);
			break;
		case 2:
			String[] nave2 = { "assets/enemigos/Enemigo02.png", "assets/enemigos/Enemigo02_2.png" };
			this.nave = loadAnimationFromFiles(nave2, 0.2f, true);
			break;
		case 3:
			String[] nave3 = { "assets/enemigos/Enemigo03.png", "assets/enemigos/Enemigo03_2.png" };
			this.nave = loadAnimationFromFiles(nave3, 0.2f, true);
			break;
		case 4:
			String[] nave4 = { "assets/enemigos/Enemigo04.png", "assets/enemigos/Enemigo04_2.png" };
			this.nave = loadAnimationFromFiles(nave4, 0.2f, true);
			break;

		default:
			break;
		}
		
		setBoundaryPolygon(10);
		
		balaActual=0;
		numBalas=10;
		this.coolDownBala=2f;
		this.cuentaCoolDownBala=0;
		balas=new ArrayList<Bala>();
		for(int i=0; i<numBalas; i++) {
			balas.add(new Bala(0,0,s,nivel,2));
		}
		this.cuentaCoolDownBala=0;
		
		
	}
	
	/**
	 * Metodo que establece la caja de colisi�n de los enemigos
	 */
	@Override
	public void setBoundaryPolygon(int numSides) {
		// TODO Auto-generated method stub
		float w = getWidth()/2;
        float h = getHeight()/2;

        float[] vertices = new float[2*numSides];
        for (int i = 0; i < numSides; i++)
        {
            float angle = i * 6.28f / numSides;
            // x-coordinate
            vertices[2*i] = w/2 * MathUtils.cos(angle) + getWidth()/2;
            // y-coordinate
            vertices[2*i+1] = h/2 * MathUtils.sin(angle) + getHeight()/2;
        }
        boundaryPolygon = new Polygon(vertices);
	}
	
	/**
	 * Metodo que se ejecutan en bucle haciendo comprobaciones para realizar diderentes acciones en consecuecia
	 */
	@Override
	public void act(float dt) {
		if (enabled) {
			super.act(dt);
			
			if(this.cuentaInvulnerable>0) {//si el personaje es invulnerable, hay que descontar tiempo de invulnerabilidad
				this.cuentaInvulnerable-=dt;
			}
			if (naveSeleccionada != 4) {
				Timer.schedule(new Task(){
				    @Override
				    public void run() {
				    		moveBy(0, velocityVec.y-aceleracion * dt);
				    }
				}, delayMovimiento);
			}
			
			if (naveSeleccionada == 1) {
				if (this.getX() >= 335) {
					izquierda = true;
					derecha = false;
				}else if(this.getX() <= -5) {
					derecha = true;
					izquierda = false;
				}
				
				if (izquierda) {
					moveBy(velocityVec.x-20 * dt, 0);
				}else if(derecha) {
					moveBy(velocityVec.x+20 * dt, 0);
				}
			}
			
			if (naveSeleccionada == 4) {
				if(nivel.jugador.getX()<this.getX()) {
					accelerationVec.add(-aceleracionPersigue,0);
					
				}else if(nivel.jugador.getX()>this.getX()) {
					accelerationVec.add(aceleracionPersigue,0);
				} 
				if(nivel.jugador.getY()<this.getY()) {
				accelerationVec.add(0,-aceleracionPersigue);
				}else if(nivel.jugador.getY()>this.getY()) {
					accelerationVec.add(0,aceleracionPersigue);
				}
				velocityVec.add(accelerationVec.x*dt, accelerationVec.y*dt);
				velocityVec.x=MathUtils.clamp(velocityVec.x, -velocidadMaxima, velocidadMaxima);
				velocityVec.y=MathUtils.clamp(velocityVec.y, -velocidadMaxima, velocidadMaxima);
				accelerationVec.set(0,0);
				moveBy(velocityVec.x*dt, velocityVec.y*dt);
			}
			
			
			if(this.cuentaCoolDownBala<=0) {
				this.disparar(1);
			}
			if(cuentaCoolDownBala>0) {//Si a�n no se ha terminado el tiempo de coolDown de disparo, se decrementa
				this.cuentaCoolDownBala-=dt;
			}
			/*
			if (this.getY() <= -10) {
				enabled = false;
			}	
			*/
		}else {
			moveBy(500, 500);
		}
		
	}
	
	/**
	 * Metodo que pinta la nave enemiga por pantalla
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if(enabled) {
			super.draw(batch, parentAlpha);
		}
		
	}
	
	/**
	 * Metodo que dispara una bala 
	 */
	private void disparar2() {
		this.cuentaDisparo=this.tiempoDisparo;
		
		float x=this.getX()-nivel.jugador.getX();
		float y=this.getY()-nivel.jugador.getY();
		Vector2 vector=new Vector2(x,y).nor();
		//balas.get(balaActual).disparar(vector.x*-1, vector.y*-1, this.getX(),this.getY());
		balaActual++;
		balaActual=balaActual%numBalas;
	}
	/**
	 * Metodo que dispara una bala hacia la nave principal o solo hacia abajo dependiendo del numero del enemigo
	 * @param direccion Direcci�n de la nave
	 */
	public void disparar(int direccion) {
		
		float x=this.getX()-nivel.jugador.getX();
		float y=this.getY()-nivel.jugador.getY();
		Vector2 vector=new Vector2(x,y).nor();
		
		//balas.get(balaActual).dispararEnemigo(direccion, this);
		if (naveSeleccionada != 2) {
			balas.get(balaActual).dispararEnemigoDirigido(vector.x*-1, vector.y*-1, this);
		}else {
			balas.get(balaActual).dispararEnemigo(-direccion, this);
		}
		
		balaActual++;
		balaActual=balaActual%numBalas;
		cuentaCoolDownBala=coolDownBala;
	}
	/**
	 * Metodo que resta vida a la nave enemiga
	 */
	public void dano() {//funci�n para hacer una cantidad concreta de da�o al personaje
		if(this.cuentaInvulnerable<=0) {
			this.vida-=dano;
			this.cuentaInvulnerable=this.tiempoInvulnerable;
		}
			
	}
	/**
	 * Metodo que devuelve la vida de la nave enemiga
	 * @return La vida la nave
	 */
	public float getVida() {
		return vida;
	}
	/**
	 * Metodo que establece la vida de la nave enemiga
	 * @param vida vida de la nave
	 */
	public void setVida(float vida) {
		this.vida = vida;
	}
	/**
	 * Metodo que devuelve true o false si la nave esta activada o no
	 * @return true o false
	 */
	public boolean isEnabled() {
		return enabled;
	}
	/**
	 * Metodo que establece si la nave esta operativa o no
	 * @param enabled true si la nave esta operativa y false si no queremos que este operativa
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	/**
	 * Metodo devuelve si la nave ha sido abatida
	 * @return true o false
	 */
	public boolean isMuerta() {
		return muerta;
	}
	
	/**
	 * Metodo que establece si la nave ha sido abatida o no
	 * @param muerta true o false
	 */
	public void setMuerta(boolean muerta) {
		this.muerta = muerta;
	}
}
