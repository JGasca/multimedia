package Principal;

import java.util.TimerTask;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
/**
 * Clase bala que contiene todo el comportamiento de las balas del juego
 * @author javig
 *
 */
public class Bala extends BaseActor{
	private Animation bala;
	private Animation explosion;
	private Animation explosion2;
	private int numAni;
	private LevelScreen nivel;
	private float tiempoBala;
	private float velocidadBala;
	private float cuentaBala;
	private int direccion;
	private float delayMovimiento;
	
	private float cuentaCoolDownBala;
	private float cuentaCoolDownBala2;

	protected float cuentaTiempo;
	private float cuentaDisparo;

	private boolean enabled;
	/**
	 * Constructor de la clase Bala
	 * @param x Posici�n en x donde se creara la bala
	 * @param y Posici�n en y donde se creara la bala
	 * @param s Stage del juego
	 * @param nivel LevelScreen del nivel
	 * @param numAnim Numero correspodiente a la animaci�n a cargar
	 */
	public Bala(float x, float y, Stage s, LevelScreen nivel, int numAnim) {
		super(x, y, s);
		this.numAni = numAnim;
		// TODO Auto-generated constructor stub
		this.nivel=nivel;
		tiempoBala=500;
		velocidadBala=50;
		this.cuentaCoolDownBala=1;
		this.cuentaCoolDownBala2=1;
		delayMovimiento = 0.7f;
		
		if (numAni == 1) {
			velocidadBala=50;
			String[] balaAnimacion = {"assets/nave/disparo/disparo1.png","assets/nave/disparo/disparo1_2.png"};
			bala = loadAnimationFromFiles(balaAnimacion, 0.2f, true);
		}else if(numAni == 2) {
			velocidadBala=35;
			String[] balaAnimacion = {"assets/enemigos/disparo/disparo2.png","assets/enemigos/disparo/disparo2_2.png"};
			bala = loadAnimationFromFiles(balaAnimacion, 0.2f, true);
		}
		
		String[] balaAnimacion = {"assets/nave/disparo/explosion1.png","assets/nave/disparo/explosion2.png",
				"assets/nave/disparo/explosion3.png","assets/nave/disparo/explosion4.png",
				"assets/nave/disparo/explosion5.png","assets/nave/disparo/explosion6.png"};
		explosion = loadAnimationFromFiles(balaAnimacion, 0.4f, true);
		explosion2 = loadAnimationFromFiles(balaAnimacion, 0.6f, false);	
	}
	
	/**
	 * Act de la clase Bala
	 */
	@Override
	public void act(float dt) {
		if(enabled) {
		super.act(dt);
		moveBy(velocityVec.x*dt,velocityVec.y*dt);
		cuentaBala-=dt;
		
		if (numAni == 2) {
			if( this.enabled && this.overlaps(nivel.jugador)) {
				//nivel.jugador.dano(10);
				setAnimation(explosion);
				this.velocityVec.x = 0;
				this.velocityVec.y = -50;
				Timer.schedule(new Task(){
				    @Override
				    public void run() {
				    	setAnimation(bala);
				    	enabled = false;
				    	
				    }
				}, delayMovimiento);
				if (!nivel.infiniteLife) {
					nivel.jugador.dano();
				}
			}
			
		}else if (numAni == 1) {
			for (NavesEnemigas enemigo : nivel.enemigos) {
				if( this.enabled && this.overlaps(enemigo)) {
					setAnimation(explosion2);
					this.velocityVec.x = 0;
					this.velocityVec.y = 20;
					
					enemigo.dano();
					
					if (this.cuentaCoolDownBala2<=0) {
						this.enabled=false;
						setAnimation(bala);
					}
					
					if(cuentaCoolDownBala2>0) {//Si a�n no se ha terminado el tiempo de coolDown de disparo, se decrementa
						this.cuentaCoolDownBala2-=dt;
					}
				}
			}
			
		}
		
		cuentaTiempo-=dt;
		if(cuentaBala<=0) {
			enabled=false;
		}
		
		}
	}
	
	/**
	 * Metodo que pinta las animaciones de las balas
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if(enabled) {
			super.draw(batch, parentAlpha);
		}
		
	}
	/**
	 * Metodo que dispara la bala del personaje principal
	 * @param direccion Direcci�n de la bala
	 */
	public void disparar(int direccion) {
		this.enabled=true;
		this.setPosition(nivel.jugador.getX()+5, nivel.jugador.getY());
		cuentaBala=tiempoBala;
		if (numAni==1) {
			this.velocityVec.set(0,velocidadBala*direccion);
		}else if(numAni == 2){
			this.velocityVec.set(0,velocidadBala*-direccion);
		}
		
		this.direccion=direccion;
	}
	/**
	 * Metodo que dispara la bala hacia abajo del mapa desde un enemigo
	 * @param direccion direcci�n de la bala
	 * @param nave Nave desde la que se dispara la bala
	 */
	public void dispararEnemigo(int direccion, NavesEnemigas nave) {
		this.enabled=true;
		this.setPosition(nave.getX()+(nave.getWidth()/2)-3, nave.getY());
		cuentaBala=tiempoBala;
		this.velocityVec.set(0,velocidadBala*direccion);
		
		this.direccion=direccion;
	}
	
	/**
	 * Metodo que dispara hacia el juegador
	 * @param x Posici�n en x del jugador
	 * @param y Posici�n en y del jugador
	 * @param nave Nave desde la que disparamos
	 */
	public void dispararEnemigoDirigido(float x, float y, NavesEnemigas nave) {
		this.enabled=true;
		this.setPosition(nave.getX()+(nave.getWidth()/2)-3, nave.getY());
		this.velocityVec.set(velocidadBala*x, velocidadBala*y);
		cuentaBala=tiempoBala;
		this.velocityVec.set(velocidadBala*x,velocidadBala*y);

	}

}