package Principal;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
/**
 * Clase Inicio que Muestra la pantalla principal con un boton de empezar el juego y otro de salir
 * @author javig
 *
 */
public class Inicio extends BaseScreen{
	private Table table;
	private Label mensaje;
	int row_height = Gdx.graphics.getHeight() / 12;
	int col_width = Gdx.graphics.getWidth() / 12;
	/**
	 * Metodo que inicializa la label con el nombre del juego y los botones de salir y comenzar el juego
	 */
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		FondoInicio fondo = new FondoInicio(0, 0, backStage);
		mensaje=new Label("AeroSpace", BaseGame.labelStyle);
		mensaje.setSize(Gdx.graphics.getWidth(),row_height);
		mensaje.setPosition(Gdx.graphics.getWidth()/2-100, 500);
		uiStage.addActor(mensaje);
		
		table=new Table();
		table.setFillParent(true);
		//Gdx.input.setInputProcessor(this.uiStage);
		this.uiStage.addActor(table);
		
		TextButton boton=new TextButton("Jugar", BaseGame.textButtonStyle);
		boton.addListener(
				(Event e)->{if(!(e instanceof InputEvent)|| !((InputEvent)e).getType().equals(Type.touchDown))
					return false;
				this.dispose();
				BaseGame.setActiveScreen(new LevelScreen());
				return false;
				});
		table.add(boton);
		
		TextButton botonSalir=new TextButton("Salir", BaseGame.textButtonStyle);
		botonSalir.addListener(
				(Event e)->{if(!(e instanceof InputEvent)|| !((InputEvent)e).getType().equals(Type.touchDown))
					return false;
				this.dispose();
				Gdx.app.exit();
				return false;
				});
		table.add(botonSalir);
		
	}
	/**
	 * Metodo que captura cualquier actualización
	 */
	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
	}
}
