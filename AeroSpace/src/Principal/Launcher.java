package Principal;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
/**
 * Clase Launcher que crea el videojuego
 * @author javig
 *
 */
public class Launcher
{
	/**
	 * Metodo main que crea una instancia de Game para iniciar el juego
	 * @param args
	 */
    public static void main (String[] args)
    {
        Game myGame = new AeroSpace(); 
        LwjglApplication launcher = new LwjglApplication( myGame, "AeroSpace", 700, 700 );
    }
}