package Principal;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
/**
 * Clase GameOverScreen que establece el mensaje del Game Over al perder en el juego
 * @author javig
 *
 */
public class GameOverScreen extends BaseScreen{
	private Label mensaje;
	/**
	 * Metodo que inicializa el mensaje de Game Over
	 */
	@Override
	public void initialize() {
		mensaje=new Label("Game Over", BaseGame.labelStyle);
		uiStage.addActor(mensaje);
		mensaje.setPosition(Gdx.graphics.getWidth()/2-100, 350);
		
	}
	/**
	 * Metodo que te devuelve el inicio a presionar la tecla enter
	 */
	@Override
	public void update(float dt) {
		if(Gdx.input.isKeyJustPressed(Keys.ENTER)) {
			this.dispose();
			BaseGame.setActiveScreen(new Inicio());
		}
		
	}
}
