package Principal;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.backends.lwjgl.audio.Mp3.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Clase LevelScreen que contiene todo el funcionamiento del videojuego
 * @author javig
 *
 */
public class LevelScreen extends BaseScreen{
	public Nave jugador;
	public ArrayList<NavesEnemigas> enemigos;
	public ArrayList<PowerUp> powerup;
	private boolean todasMuertas;
	
	private Sound disparo;
	private Music musica;
	private float volumenSound;
	private float volumenMusic;
	
	private Label vida;
	private Label nivel;
	protected boolean infiniteLife;
	
	/**
	 * Metodo initialize que inicia las varibles y pinta en pantalla la nave principal, las naves enemigas y los powerUps 
	 */
	@Override
	public void initialize() {
		Fondo fondo = new Fondo(0, 0, backStage);
		
		((OrthographicCamera)this.mainStage.getCamera()).setToOrtho(false, 120*3, 120*3);
		
		enemigos = new ArrayList<NavesEnemigas>();
		powerup = new ArrayList<PowerUp>();
		infiniteLife = false;
		
		jugador = new Nave((float)160, (float)50, mainStage, this);
		disparo = Gdx.audio.newSound(Gdx.files.internal("assets/sonidos/disparo.mp3"));
		musica = (Music) Gdx.audio.newMusic(Gdx.files.internal("assets/sonidos/Musica.mp3"));
		volumenSound = 0.3f;
		volumenMusic = 0.7f;
		
		musica.setLooping(true);
		musica.setVolume(volumenMusic);
		musica.play();
		
		if (Parametros.nivel % 5 == 0) {
			Parametros.numEnemigos = Parametros.numEnemigos + 2;
		}
		
		todasMuertas = false;
		NavesEnemigas enemigo;
		int numNave = 1;
		for (int i = 0; i < Parametros.numEnemigos; i++) {
			int randomx = (int) (Math.random()*(330-5+1)+5);
			int randomy = (int) (Math.random()*(430-360+1)+360);
			enemigo= new NavesEnemigas(randomx, randomy, mainStage, this, Parametros.getVidaEnemigos(), numNave);
			enemigos.add(enemigo);
			numNave++;
			if (numNave == 5) {
				numNave = 1;
			}
		}
		
		PowerUp poder = new PowerUp(30, 190, mainStage, this, 1);
		powerup.add(poder);
		PowerUp poder2 = new PowerUp(300, 190, mainStage, this, 2);
		powerup.add(poder2);
		
		
		vida=new Label("Vida: ", BaseGame.labelStyle);
		this.nivel=new Label("Nivel: ", BaseGame.labelStyle);
		vida.setColor(Color.RED);
		this.nivel.setColor(Color.GRAY);
		Label espacio=new Label(" ",BaseGame.labelStyle);
		
		
		uiTable.add(nivel).left();
		uiTable.add(espacio).right().expandX();
		uiTable.row();
		uiTable.add(vida).left();
		
		uiTable.row();
		uiTable.row();
		uiTable.add(espacio).expandY();

		
	}

	/**
	 * Metodo que va comprobando una serie de parametros en bucle
	 */
	@Override
	public void update(float dt) {
		if (jugador.vida<=0) {
			this.gameOver();
		}
		int navesDestruidas = 0;
		for (NavesEnemigas nave : enemigos) {
			if (nave.getY() <= -10 && nave.isEnabled()) {
				nave.setEnabled(false);
				this.gameOver();
			}
			if(nave.getVida() <= 0) {
				nave.setEnabled(false);
				nave.setMuerta(true);
			}
			if (!nave.isEnabled() && nave.isMuerta()) {
				navesDestruidas++;
			}
		}
		
		if (navesDestruidas == enemigos.size()) {
			nextLevel();
		}
		
		if (!infiniteLife) {
			this.vida.setText("Vida: "+(int)jugador.vida);
		}else {
			this.vida.setText("Vida: infinita");
		}
		
		this.nivel.setText("Nivel: "+Parametros.nivel);
		
	}
	/**
	 * Metodo que hacer sonar el sonido de disparo
	 */
	public void sonidoDisparo() {
		disparo.play(volumenSound);
	}
	/**
	 * Metodo que resetea todos los parametros por defecto, apaga todos los sonidos, cierra el LevelScreen e inicia GameOverScreen
	 */
	public void gameOver() {
		//this.theme.dispose();
		Parametros.resetParametros();
		this.disparo.dispose();
		musica.dispose();
		this.dispose();
		BaseGame.setActiveScreen(new GameOverScreen());
	}
	/**
	 * Metodo que apaga todos los sonidos, a�ade mas vida a los enemigos y a la nave principla , suma uno al nivel en parametros he inicializa la clase EntrePantalla
	 */
	public void nextLevel() {
		this.disparo.dispose();
		musica.dispose();
		this.dispose();
		Parametros.setVidaEnemigos(Parametros.getVidaEnemigos()+50);
		Parametros.setVidaNave(Parametros.getVidaNave()+20);
		Parametros.SumUnoNivel();
		
		BaseGame.setActiveScreen(new EntrePantalla());
	}
	
	

}
