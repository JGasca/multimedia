package Principal;
import com.badlogic.gdx.scenes.scene2d.Stage;
/**
 * Clase que contiene el fondo del videojuego
 * @author javig
 *
 */
public class Fondo extends BaseActor{
	/**
	 * Constructor de la clase fondo que carga la imagen
	 * @param x posici�n en x 
	 * @param y Posici�n en y
	 * @param s Stage del nivel
	 */
	public Fondo(float x, float y, Stage s) {
		super(x, y, s);
		// TODO Auto-generated constructor stub
		loadTexture("assets/fondo/fondo2.jpg");
	}

}
