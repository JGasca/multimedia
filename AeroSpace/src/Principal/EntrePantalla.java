package Principal;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
/**
 * Clase EntrePantalla que muestra el nivel que vas a empezar 
 * @author javig
 *
 */
public class EntrePantalla extends BaseScreen{
	private Label mensaje;
	/**
	 * Metodo que inicializa el mensaje que se mostrara
	 */
	@Override
	public void initialize() {
		mensaje=new Label("Nivel " + Parametros.nivel, BaseGame.labelStyle);
		uiStage.addActor(mensaje);
		mensaje.setPosition(Gdx.graphics.getWidth()/2-70, 350);
		
	}
/**
 * Metodo que esta se ejeucta cuando se presiona el la tecla Enter
 */
	@Override
	public void update(float dt) {
		if(Gdx.input.isKeyJustPressed(Keys.ENTER)) {
			BaseGame.setActiveScreen(new LevelScreen());
		}
	}
}
