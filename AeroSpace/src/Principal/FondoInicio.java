package Principal;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.compression.lzma.Base;
/**
 * Clase FondoInicio que establece el fondo de la pantalla de inicio
 * @author javig
 *
 */
public class FondoInicio extends BaseActor{
	/**
	 * Constructor de la clase FondoInicio
	 * @param x Posici�n en x
	 * @param y Posici�n en y
	 * @param s Stage del nivel
	 */
	public FondoInicio(float x, float y, Stage s) {
		super(x, y, s);
		// TODO Auto-generated constructor stub
		loadTexture("assets/fondo/fondo3.PNG");
	}

}
