package Principal;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.scenes.scene2d.Stage;
/**
 * Clase Nave que se utiliza para el personaje principal que es la Nave principal
 * @author javig
 *
 */
public class Nave extends BaseActor{
	private Animation parado;
	private Animation izquierda;
	private Animation izquierdaMedio;
	private Animation derecha;
	private Animation derechaMedio;
	
	private ArrayList<Bala> balas;
	private int numBalas;
	private float coolDownBala;
	private float cuentaCoolDownBala;
	private int balaActual;
	public float velocidadColision;
	
	private float aceleracionNave;
	private float deceleracionNave;
	private float velocidadMaximaNave;
	private ShapeRenderer shapeRenderer;
	
	private LevelScreen nivel;
	
	private float alturaPies;
	private float offsetPies;
	
	private boolean enabled;
	
	public float vida;
	private float cuentaInvulnerable;
	private float tiempoInvulnerable; 
	
	private int dano;
	
	
	/**
	 * Constructor de la clase Nave que inicializa las varibles y establece las animaciones
	 * @param x Posici�n en x donde aparecera la nave
	 * @param y	Posici�n en y donde aparecera la nave
	 * @param s Stage del nivel
	 * @param nivel LevelScreen del nivel
	 */
	public Nave(float x, float y, Stage s,LevelScreen nivel) {
		
		super(x, y, s);
		this.nivel = nivel;
		
		
		setWorldBounds(360,360);
		
		String[] NaveParada = {"assets/nave/Parada/Parada1.png","assets/nave/Parada/Parada2.png"};
		parado = loadAnimationFromFiles(NaveParada, 0.2f, true);
		
		String[] NaveDerecha = {"assets/nave/derecha/derecha1.png","assets/nave/derecha/derecha2.png"};
		derecha = loadAnimationFromFiles(NaveDerecha, 0.2f, true);
		String[] NaveDerechaMedio = {"assets/nave/derecha/derechaMedio1.png","assets/nave/derecha/derechaMedio2.png"};
		derechaMedio = loadAnimationFromFiles(NaveDerechaMedio, 0.2f, false);
		
		String[] NaveIzquierda = {"assets/nave/izquierda/izquierda1.png","assets/nave/izquierda/izquierda2.png"};
		izquierda = loadAnimationFromFiles(NaveIzquierda, 0.2f, true);
		String[] NaveIzquierdaMedio = {"assets/nave/izquierda/izquierda1.png","assets/nave/izquierda/izquierda2.png"};
		izquierdaMedio = loadAnimationFromFiles(NaveIzquierdaMedio, 0.2f, false);
		
		setBoundaryPolygon(10);
		
		enabled = true;
		velocidadMaximaNave=100;
		aceleracionNave=100;
		deceleracionNave=200;
		alturaPies=6;
		offsetPies=10;
		vida = Parametros.getVidaNave();
		cuentaInvulnerable=0;
		tiempoInvulnerable=0.3f;
		
		balaActual=0;
		numBalas=300;
		dano = 10;
		this.coolDownBala=0.3f;
		this.cuentaCoolDownBala=0;
		balas=new ArrayList<Bala>();
		for(int i=0; i<numBalas; i++) {
			balas.add(new Bala(0,0,s,nivel,1));
		}
		
		shapeRenderer = new ShapeRenderer();
		
		
	
	}
	
	
	/**
	 * Metodo que establece la caja de colisi�n de la nave
	 */
	@Override
	public void setBoundaryPolygon(int numSides) {
		// TODO Auto-generated method stub
		float w = getWidth()/5;
        float h = getHeight()/5;

        float[] vertices = new float[2*numSides];
        for (int i = 0; i < numSides; i++)
        {
            float angle = i * 6.28f / numSides;
            // x-coordinate
            vertices[2*i] = w/2 * MathUtils.cos(angle) + getWidth()/2;
            // y-coordinate
            vertices[2*i+1] = h/2 * MathUtils.sin(angle) + getHeight()/2;
        }
        boundaryPolygon = new Polygon(vertices);
	}



	public void act(float dt) {
		if (enabled) {
			super.act(dt);
			
			if(this.cuentaInvulnerable>0) {//si el personaje es invulnerable, hay que descontar tiempo de invulnerabilidad
				this.cuentaInvulnerable-=dt;
			}
			
			if((Gdx.input.isKeyPressed(Keys.W) || Gdx.input.isKeyPressed(Keys.UP)) ){
				accelerationVec.add(0, aceleracionNave);
			}
			if(Gdx.input.isKeyPressed(Keys.S) || Gdx.input.isKeyPressed(Keys.DOWN)){
				accelerationVec.add(0, -aceleracionNave);
			}
			if(Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.LEFT)){
				accelerationVec.add(-aceleracionNave,0 );
			}
			if(Gdx.input.isKeyPressed(Keys.D) || Gdx.input.isKeyPressed(Keys.RIGHT)){
				accelerationVec.add(aceleracionNave, 0);
			}
			if(Gdx.input.isKeyPressed(Keys.SPACE) && this.cuentaCoolDownBala<=0) {
				nivel.sonidoDisparo();
				this.disparar(1);
			}
			
			if(cuentaCoolDownBala>0) {//Si a�n no se ha terminado el tiempo de coolDown de disparo, se decrementa
				this.cuentaCoolDownBala-=dt;
			}
			
			if(!Gdx.input.isKeyPressed(Keys.A) && !Gdx.input.isKeyPressed(Keys.D) && !Gdx.input.isKeyPressed(Keys.LEFT) 
					&& !Gdx.input.isKeyPressed(Keys.RIGHT) && !Gdx.input.isKeyPressed(Keys.W) && !Gdx.input.isKeyPressed(Keys.UP) && !Gdx.input.isKeyPressed(Keys.S) && !Gdx.input.isKeyPressed(Keys.DOWN)){
				float cantidadDecelerando=deceleracionNave*dt;
				float direccionNaveX;
				float direccionNaveY;
				if(velocityVec.x>0)
					direccionNaveX=1;
				else
					direccionNaveX=-1;
				if(velocityVec.y>0)
					direccionNaveY=1;
				else
					direccionNaveY=-1;
				
				float velocidadAndandoX=Math.abs(velocityVec.x);
				float velocidadAndandoY=Math.abs(velocityVec.y);
				velocidadAndandoX-=cantidadDecelerando;
				velocidadAndandoY-=cantidadDecelerando;
		
				if(velocidadAndandoX<0)
					velocidadAndandoX=0;
				velocityVec.x=velocidadAndandoX*direccionNaveX;
				velocityVec.y=velocidadAndandoY*direccionNaveY;
				
				//cantidadDecelerando=5*dt;
				
				
				   //Esto es lo que hay que ver para que frene bien 
				
			}
			
			if(velocityVec.x==0) {
				setAnimation(parado);
			}
			else if (velocityVec.x>0) {
				setAnimation(derechaMedio);
				setAnimation(derecha);
			}else if (velocityVec.x<0) {
				setAnimation(izquierdaMedio);
				setAnimation(izquierda);
			}
			//accelerationVec.add(0,-Parametros.getGravedad());
			
			
			velocityVec.add(accelerationVec.x*dt, accelerationVec.y*dt);
			velocityVec.x=MathUtils.clamp(velocityVec.x, -velocidadMaximaNave, velocidadMaximaNave);
			
			
			moveBy(velocityVec.x*dt, velocityVec.y*dt);
			
			accelerationVec.set(0,0);
			//pies.setPosition(this.getX()+offsetPies/2, this.getY()-alturaPies);
			
			
			boundToWorld();
			//alignCamera();
			//wrapAroundWorld();
		}
		
	}
	
	/**
	 * Metodo que dibuja el personaje en la pantalla
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (enabled) {
			super.draw(batch, parentAlpha);
			batch.end();
			shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
			shapeRenderer.begin(ShapeType.Line);
			shapeRenderer.setColor(Color.WHITE);
			if(this.getBoundaryPolygon()!=null) {
			float[] vertices =	new float[this.getBoundaryPolygon().getVertices().length];
					
			for(int i=0;i<vertices.length/2;i++) {
				//vertices[2*i]=this.getBoundaryPolygon().getVertices()[2*i]+this.getX();
				//vertices[2*i+1]=this.getBoundaryPolygon().getVertices()[2*i+1]+this.getY();
				
			}
			
			shapeRenderer.polygon(vertices);
			}
			shapeRenderer.end();	
			batch.begin();
		}
		
		
		
	}
	
	/**
	 * Metodo que dispara balas
	 * @param direccion direcci�n de las balas
	 */
	public void disparar(int direccion) {
		balas.get(balaActual).disparar(direccion);
		balaActual++;
		balaActual=balaActual%numBalas;
		cuentaCoolDownBala=coolDownBala;
	}
	/**
	 * Metodo que baja la vida
	 * 
	 */
	public void dano() {
		if(this.cuentaInvulnerable<=0) {
			this.vida-=dano;
			this.cuentaInvulnerable=this.tiempoInvulnerable;
		}
			
	}
	/**
	 * Metodo que devuelve la aceleraci�n de la nave
	 * @return La aceleraci�n de la nave
	 */
	public float getAceleracionNave() {
		return aceleracionNave;
	}
	/**
	 * Metodo que establece la aceleraci�n de la nave
	 * @param aceleracionNave float aceleracion de la nave
	 */
	public void setAceleracionNave(float aceleracionNave) {
		this.aceleracionNave = aceleracionNave;
	}


	
	
}
