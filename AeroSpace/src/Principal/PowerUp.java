package Principal;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
/**
 * Clase PowerUp que contiene los poderes que estan disponibles
 * @author javig
 *
 */
public class PowerUp extends BaseActor{
	private int opcion;
	private Animation animacion;
	private boolean enabled;
	private LevelScreen nivel;
	private float delay;
	private float cont;

	/**
	 * Constructor de la clase PowerUp
	 * @param x Posici�n en x 
	 * @param y Posici�n en y 
	 * @param s Stage
	 * @param nivel nivel en el que estamos
	 * @param opcion opcion de poder 1 o 2
	 */
	public PowerUp(float x, float y, Stage s,LevelScreen nivel, int opcion) {
		super(x, y, s);
		enabled = true;
		cont =0;
		delay = 5;
		this.opcion = opcion;
		this.nivel = nivel;
		String[] powerup = {"assets/powerup/powerUp1.png","assets/powerup/powerUp2.png"};
		animacion = loadAnimationFromFiles(powerup, 0.2f, true);
	}
	/**
	 * metodo act que se ejecuta en bucle haciendo comprobaciones 
	 */
	public void act(float dt) {
		if (enabled) {
			super.act(dt);
			
			setBoundaryPolygon(10);
			
			if (enabled && this.overlaps(nivel.jugador)) {
				if (opcion == 1) {
					 // seconds
					for (NavesEnemigas enemigos : nivel.enemigos) {
						enemigos.dano = enemigos.dano * Parametros.nivel;
					}
					nivel.jugador.setAceleracionNave(nivel.jugador.getAceleracionNave()*2);
					Timer.schedule(new Task(){
					    @Override
					    public void run() {
					    	for (NavesEnemigas enemigos : nivel.enemigos) {
								enemigos.dano = enemigos.dano / Parametros.nivel;
							}
					        nivel.jugador.setAceleracionNave(nivel.jugador.getAceleracionNave()/2);
					    }
					}, delay);
					
				}else if (opcion == 2) {
					nivel.infiniteLife = true;
					Timer.schedule(new Task(){
					    @Override
					    public void run() {
					        nivel.infiniteLife = false;
					    }
					}, delay);
				}
				enabled = false;
			}
		}
	}
	
	/**
	 * Metodo que establece la caja de colisi�n del PowerUp
	 */
	@Override
	public void setBoundaryPolygon(int numSides) {
		// TODO Auto-generated method stub
		float w = getWidth()*2;
        float h = getHeight()*2;

        float[] vertices = new float[2*numSides];
        for (int i = 0; i < numSides; i++)
        {
            float angle = i * 6.28f / numSides;
            // x-coordinate
            vertices[2*i] = w/2 * MathUtils.cos(angle) + getWidth()/2;
            // y-coordinate
            vertices[2*i+1] = h/2 * MathUtils.sin(angle) + getHeight()/2;
        }
        boundaryPolygon = new Polygon(vertices);
	}

	/**
	 * Metodo que pinta el PowerUp por pantalla
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (enabled) {
			super.draw(batch, parentAlpha);
		}
	}
	
	

}
