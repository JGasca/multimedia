package Principal;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
/**
 * Clase Enemigo
 * @author javig
 *
 */
public class Enemigo extends BaseActor{
	public boolean vivo;
	protected LevelScreen nivel;
	/**
	 * Constructor de la clase Enemigo
	 * @param x Posici�n en x del enemigo
	 * @param y Posici�n en y del enemigo
	 * @param s Stage del nivel
	 * @param nivel Pantalla del nivel
	 */
	public Enemigo(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s);
		// TODO Auto-generated constructor stub
		this.nivel=nivel;
		vivo=true;
	}
	/**
	 * Metodo que mata al enemigo
	 */
	public void dano() {
		this.vivo=false;
	}
/**
 * Metodo que pinta el enemigo en la pantalla
 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if(vivo)super.draw(batch, parentAlpha);
	}
	
	
}
