package Principal;
/**
 * Clase que inicia la pantalla principal del juego
 * @author javig
 *
 */
public class AeroSpace  extends BaseGame{
	public void create() {
		super.create();
		setActiveScreen(new Inicio());
	}
}
